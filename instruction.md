# TP DOCKER

For the start let's install the docker images necessary for the rest of the project:

    docker pull httpd
	docker pull openjdk
	docker pull postgres
    docker pull adminer

create basic docker file for postgres, passing env args in the docker run command : 

	FROM postgres:11.6-alpine

	COPY 01-CreateScheme.sql /docker-entrypoint-initdb.d
	COPY 02-InsertData.sql /docker-entrypoint-initdb.d

	ENV POSTGRES_DB=db


then after, type the following commands:
```
docker build -t vincent.vanhassel/basicpostgres .

docker run --name basicpostgres -v /postgres/data:/var/lib/postgresql/data -p 8000:8000 -e POSTGRES_USER=vince -e POSTGRES_PASSWORD=password vincent.vanhassel/basicpostgre:latest

docker run --link 22a5ed757c71:db -p 8080:8080 adminer
```

---
### For the jdk part

	FROM openjdk:7

	COPY . /usr/src/myapp
	WORKDIR /usr/src/myapp
	RUN javac Main.java
	CMD ["java", "Main"]

docker build -f ./DockerFile -t vincent.vanhassel/backend:latest .
docker run -it --rm --name backend vincent.vanhassel/backend:latest

---

### maven api demo

docker build -f ./DockerFile -t vincent.vanhassel/test .
docker run -it --name javademo vincent.vanhassel/test 

---

controller :

the student controller is used to manage student accounts (CRUD), it can also return a student via his id

the greeting controller allows to return a message with his username

the departement controller will return the student lists, the number, according to the departments, see even the departments


entity allows to create models or classes with which we can interact

the services allows to use function that controllers can call to execute an action

for me the services are the functions that the controllers will call in order to execute the actions


---
---
Full command:

1. Create and run a dockerfile (Init database):
	1. docker pull httpd
	2. docker pull openjdk
	3. docker pull postgres

2. Create and run a dockerfile (Init database):
	4. Vim dockerfile (création du dockerfile)
	5. docker build -t 'test' . 
	6. docker run test
	7. docker pull adminer
	8. docker run admirer
	9. vim 01-CreateScheme.sql
	10. vim 02-InsertData.sql
	11. vim docker-compose.yml
	12. docker build -t 'test' .
	13. docker run test

3. Persist Data:
	14. docker run -v /data:/var/lib/postgresql/data

4. Backend API
	15. Mkdir back-end api && cd backend-api
	16. Vim main.java
	17. vim dockerfile
   FROM openjdk:11 
   WORKDIR /usr/src/ 
   COPY Main.java . 
   RUN javac Main.java RUN pwd && ls 
   FROM openjdk:11-jre 
   WORKDIR /usr/src/ COPY —from=0 /usr/src/Main.class . 
   RUN java Main
	18. https://start.spring.io/ —> 
	19. Ask yourself: Are you able to contact your SpringBook application through a web browser?